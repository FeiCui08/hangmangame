#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: fei
"""

import time
import random
import string
import unittest


class Hangman:
	def __init__(self, word, max_trials=6):
		self.word = word
		self.max_trials = max_trials
		self.correct_letters = {}
		self.wrong_letters = set()
		if word == "":
			raise ValueError("Empty word!")
	
	def is_won(self):
		return set(self.word) == set(self.correct_letters.keys())

	def is_lost(self):
		return len(self.wrong_letters) == self.max_trials

	def is_game_over(self):
		return self.is_won() or self.is_lost()

	def submit_letter(self, letter):
		if self.is_game_over():
			return
		if not (len(letter)==1 and (ord("a")<=ord(letter)<=ord("z") or (ord("0")<=ord(letter)<=ord("9")))):
			raise ValueError("Invalid letter!")
		if letter in self.word:
			self.correct_letters[letter] = [i for i,char in enumerate(self.word) if char==letter]
		else:
			self.wrong_letters.add(letter)
	
	@staticmethod
	def from_dict(data):
		hm = Hangman(data["word"])
		hm.max_trials = data["max_trials"]
		hm.correct_letters = data["correct_letters"]
		hm.wrong_letters = set(data["wrong_letters"])
		return hm

	def to_dict(self):
		return {
			"word": self.word,
			"max_trials": self.max_trials,
			"correct_letters": self.correct_letters,
			"wrong_letters": list(self.wrong_letters)
		}	
	
class HangmanTest(unittest.TestCase):
	@staticmethod
	def random_word(length=10):
		return "".join(random.choice(string.ascii_lowercase) for c in range(length))

	def assertWon(self, hangman):
		self.assertTrue(hangman.is_won())
		self.assertFalse(hangman.is_lost())
		self.assertTrue(hangman.is_game_over())

	def assertLost(self, hangman):
		self.assertFalse(hangman.is_won())
		self.assertTrue(hangman.is_lost())
		self.assertTrue(hangman.is_game_over())

	def assertInGame(self, hangman):
		self.assertFalse(hangman.is_won())
		self.assertFalse(hangman.is_lost())
		self.assertFalse(hangman.is_game_over())

	def test_empty_word(self):
		with self.assertRaises(ValueError):
			hm = Hangman("")
				
	def test_submit_correct(self):
		word = HangmanTest.random_word()
		hm = Hangman(word)
		correct_subset = list(set(word))
		for letter in correct_subset:
			hm.submit_letter(letter)
			self.assertIn(letter, hm.correct_letters)
		self.assertTrue(len(hm.wrong_letters)==0)
		self.assertWon(hm)
	
	def test_submit_wrong(self):
		word = HangmanTest.random_word()
		hm = Hangman(word)
		wrong_subset = list(set(string.ascii_lowercase) - set(word))
		for i,letter in enumerate(wrong_subset):
			if i <6:
				hm.submit_letter(letter)
				self.assertIn(letter, hm.wrong_letters)
			else:
				self.assertTrue(len(hm.correct_letters)==0)
				self.assertLost(hm)
	
if __name__=="__main__":
	seed = int(time.time())
	random.seed(seed)
	print("seed:", seed)
	unittest.main()

