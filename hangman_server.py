#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: fei
"""

import random
from flask import abort, Flask, jsonify, render_template, request, session
from hangman import Hangman

app = Flask(__name__)
app.secret_key = b'abcdefg'
words_list = ["3dhubs", "marvin", "print", "filament", "order", "layer"]

def get_current_state(hangman):
	if hangman.is_won():
		result = "win"
	elif hangman.is_lost():
		result = "lose"
	else:
		result = "ongoing"
	state = {"word_length":len(hangman.word),
			"result":result,
			"correct_letters":hangman.correct_letters,
			"wrong_letters":list(hangman.wrong_letters)}
	if hangman.is_game_over():
		state["word"] = hangman.word
	return state		
	

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/api/new_game", methods=["POST"])
def new_game():
	hm = Hangman(word=random.choice(words_list))
	session["hangman"] = hm.to_dict()
	return jsonify(get_current_state(hm))
	
@app.route("/api/current_state", methods=["GET"])
def current_state():
	hm = None
	if "hangman" in session:
		hm = Hangman.from_dict(session["hangman"])
	if hm: 
		return jsonify(get_current_state(hm))
	else:
		return new_game()

@app.route("/api/submit_letter", methods=["POST"])
def submit_letter():
	hm = Hangman.from_dict(session["hangman"])
	try:
		hm.submit_letter(request.form["submitted_letter"])
		session["hangman"] = hm.to_dict()
		return jsonify(get_current_state(hm))
	except:
		return abort(400) #bad request

if __name__=='__main__':
    app.run(debug=True)
